---
description: >-
  Expand this category to view documentation for Legacy versions of
  PlayerServers (older than v3)
---

# 👵 Legacy (v2.x)

{% content-ref url="../" %}
[..](../)
{% endcontent-ref %}

{% content-ref url="permissions-and-commands.md" %}
[permissions-and-commands.md](permissions-and-commands.md)
{% endcontent-ref %}

{% content-ref url="installation.md" %}
[installation.md](installation.md)
{% endcontent-ref %}
